package com.elrsoft.streets.util;

/**
 * Created by hanz on 05.10.2016.
 */
public class Resource {
    public final static String START_FRAGMENT = "StartFragment";
    public final static String POSITION_FRAGMENT = "PositionFragment";
    public final static String PLACES_FRAGMENT = "PlacesFragment";
    public final static String SHELUDE_FRAGMENT = "ScheduleFragment";
    public final static String ROUTE_FRAGMENT = "RouteFragment";
    public final static String SETTINGS_FRAGMENT = "SettingsFragment";
    public final static String ABOUT_FRAGMENT = "AboutFragment";
}
