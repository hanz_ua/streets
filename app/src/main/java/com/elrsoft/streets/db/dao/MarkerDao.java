package com.elrsoft.streets.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.streets.db.Resource;
import com.elrsoft.streets.db.dao.core.Dao;
import com.elrsoft.streets.objects.Marker;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by dvrez on 03/12/17.
 */
public class MarkerDao implements Dao<Marker> {
    
    SQLiteDatabase sqLiteDatabase;

    public MarkerDao(SQLiteDatabase sqLiteDatabase) { this.sqLiteDatabase = sqLiteDatabase; }


    // add new marker to DB
    @Override
    public long addMarker(Marker marker) {
        return sqLiteDatabase.insert(
                Resource.Marker.TABLE_NAME,null,
                setMarker(marker));
    }

    // creates content for adding marker
    private ContentValues setMarker(Marker marker) {
        ContentValues cv = new ContentValues();

        cv.put(Resource.Marker.LAT, marker.getLat());
        cv.put(Resource.Marker.LON, marker.getLon());
        cv.put(Resource.Marker.IMAGE, marker.getImage());
        cv.put(Resource.Marker.TEXT, marker.getText());
        cv.put(Resource.Marker.LINK, marker.getLink());

        return cv;
    }

    // request from database all markers
    @Override
    public Collection<Marker> getMarkers() {
        Cursor cursor = sqLiteDatabase.rawQuery(
                "select * from "
                + Resource.Marker.TABLE_NAME,null);
        return parseCursor(cursor);
    }

    // relate to getMarkers method
    @Override
    public Collection<Marker> parseCursor(Cursor cursor) {
        Collection<Marker> markers = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()){
            do{
                long id = cursor.getLong(cursor.getColumnIndex(Resource.Marker.ID));
                long lat = cursor.getLong(cursor.getColumnIndex(Resource.Marker.LAT));
                long lon = cursor.getLong(cursor.getColumnIndex(Resource.Marker.LON));
                String image = cursor.getString(cursor.getColumnIndex(Resource.Marker.IMAGE));
                String text = cursor.getString(cursor.getColumnIndex(Resource.Marker.TEXT));
                String link = cursor.getString(cursor.getColumnIndex(Resource.Marker.LINK));
                markers.add(new Marker(id,lat,lon,image,text,link));
            }while (cursor.moveToNext());
        }
        return markers;
    }

    public void deleteMarker(Marker marker){
        sqLiteDatabase.delete(
                Resource.Marker.TABLE_NAME,
                Resource.Marker.ID + " = ?",
                new String[]{String.valueOf(marker.getId())}
        );
    }

}