package com.elrsoft.streets.db.dao.core;

import android.database.Cursor;

import java.util.Collection;

/**
 * Created by dvrez on 03/12/17.
 */
public interface Dao<T> {

    long addMarker(T t);
    Collection<T> getMarkers();
    Collection<T> parseCursor(Cursor cursor);
}
