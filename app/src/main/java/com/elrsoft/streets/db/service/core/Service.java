package com.elrsoft.streets.db.service.core;

import java.util.Collection;

/**
 * Created by dvrez on 03/12/17.
 */
public interface Service<T> {

    long addMarker(T t);
    Collection<T> getMarkers();
}
