package com.elrsoft.streets.db.service;

import android.app.Activity;

import com.elrsoft.streets.db.dao.MarkerDao;
import com.elrsoft.streets.db.service.core.OpenDBService;
import com.elrsoft.streets.db.service.core.Service;
import com.elrsoft.streets.objects.Marker;

import java.util.Collection;

/**
 * Created by dvrez on 03/12/17.
 */
public class MarkerService extends OpenDBService implements Service<Marker> {

    private Activity activity;

    public MarkerService(Activity activity) {
        this.activity = activity;
    }


    // adds marker to DB using MarkerDao by sending activity
    @Override
    public long addMarker(Marker marker) {
        try {
            if (!isDBOpen()) {
                openDB(activity);
            }
            return new MarkerDao(getSqLiteDatabase()).addMarker(marker);
        } finally {
            if (isDBOpen()) {
                closeDB();
            }
        }
    }

    // gets markers from DB using MarkerDao by sending activity
    @Override
    public Collection getMarkers() {
        try {
            if (!isDBOpen()) {
                openDB(activity);
            }
            return new MarkerDao(getSqLiteDatabase()).getMarkers();
        } finally {
            if (isDBOpen()) {
                closeDB();
            }
        }

    }

    public void deleteMarker(Marker marker) {
        try {
            if (!isDBOpen()) {
                openDB(activity);
            }
            new MarkerDao(getSqLiteDatabase()).deleteMarker(marker);
        } finally {
            if (isDBOpen()) {
                closeDB();
            }
        }

    }

}
