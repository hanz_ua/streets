package com.elrsoft.streets.db;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dvrez on 03/12/17.
 */
public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(Activity activity) {
        super(activity, Resource.DB_NAME, null, Resource.DB_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Resource.Marker.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
