package com.elrsoft.streets.db;

/**
 * Created by dvrez on 03/12/17.
 */
public class Resource {

    // i've named database globally  because  maybe will be added other tables
    public static final String DB_NAME = "app_db_streets";
    public static final int DB_VER = 1;

    public static final class Marker {
        public static final String ID = "id";
        public static final String LAT = "lat";  // latitude
        public static final String LON = "lon";  // longitude
        public static final String IMAGE = "image";
        public static final String TEXT = "text";
        public static final String LINK = "link";
        public static final String TABLE_NAME = "markers";

        public static final String CREATE_TABLE = "create table " + TABLE_NAME
                + " ( " + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LAT + " REAL, "
                + LON + " REAL, "
                + IMAGE + " TEXT(255), "
                + TEXT + " TEXT(255), "
                + LINK + " TEXT(255)"
                + " ); ";
    }
}
