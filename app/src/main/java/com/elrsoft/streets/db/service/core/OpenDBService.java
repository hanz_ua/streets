package com.elrsoft.streets.db.service.core;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;

import com.elrsoft.streets.db.DBHelper;

/**
 * Created by dvrez on 03/12/17.
 */
public class OpenDBService {

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;

    public SQLiteDatabase getSqLiteDatabase(){ return sqLiteDatabase; }

    // checks if DB is opened
    protected boolean isDBOpen(){
        return     sqLiteDatabase != null
                && dbHelper != null
                && sqLiteDatabase.isOpen();
    }

    // opens DB but need activity as a parametr
    protected void openDB(Activity activity){
        if(sqLiteDatabase == null || !sqLiteDatabase.isOpen()){
            dbHelper = new DBHelper(activity);
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }
    }

    // closes DB
    protected void closeDB(){
        if(dbHelper != null){
            dbHelper.close();
        }
    }
}
