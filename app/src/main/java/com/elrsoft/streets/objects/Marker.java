package com.elrsoft.streets.objects;

/**
 * Created by dvrez on 03/12/17.
 */
public class Marker {

    private long id;
    private long lat;
    private long lon;
    private String image;
    private String text;
    private String link;

    // constructor with id
    public Marker(long id, long lat, long lon, String image, String text, String link) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.image = image;
        this.text = text;
        this.link = link;
    }

    // constructor without id
    public Marker(long lat, long lon, String image, String text, String link) {
        this.lat = lat;
        this.lon = lon;
        this.image = image;
        this.text = text;
        this.link = link;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public long getLon() {
        return lon;
    }

    public void setLon(long lon) {
        this.lon = lon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


}
