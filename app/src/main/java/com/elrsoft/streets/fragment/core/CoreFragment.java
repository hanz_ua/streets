package com.elrsoft.streets.fragment.core;

import android.app.Activity;
import android.app.Fragment;


import com.elrsoft.streets.activity.StartActivity;
import com.elrsoft.streets.activity.core.OnBackPressedListener;

import rx.functions.Action1;

/**
 * Created by hanz on 04.07.2016.
 */
public abstract class CoreFragment extends Fragment implements OnBackPressedListener {
    private Action1<String> onNextAction = s -> backPressed();

    protected String playList = "";

    @Override
    public void onAttach(Activity activity) {
        ((StartActivity) getActivity()).setBackPressedListener(onNextAction);
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        ((StartActivity) getActivity()).removeBackPressedListener(onNextAction);
        super.onDetach();
    }

    protected boolean isVisiblePreFragment() {
        return ((StartActivity) getActivity()).backPressedListeners.size() > 1;
    }

}
