package com.elrsoft.streets.fragment;

import com.elrsoft.streets.R;
import com.elrsoft.streets.controller.NavigationFragmentController;
import com.elrsoft.streets.fragment.core.CoreFragment;
import com.elrsoft.streets.util.Resource;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_start)
public class StartFragment extends CoreFragment {


    @Override
    public void backPressed() {
        getActivity().finish();
    }

    @AfterInject
    public void initTools() {
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    //замість AboutFragment_ вставити фрагмент з карти
                    //а замість Resource.ABOUT_FRAGMENT, вставити ресурс того фрагмента

                    NavigationFragmentController.moveToNextFragment(getFragmentManager(),
                            R.id.mainContainer, PositionFragment_.builder().build(), Resource.POSITION_FRAGMENT);
                }
            }
        };
        timerThread.start();
    }

    @AfterViews
    public void init() {
    }

}
