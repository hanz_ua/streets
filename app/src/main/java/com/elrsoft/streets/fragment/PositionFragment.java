package com.elrsoft.streets.fragment;

import android.util.Log;
import android.widget.Toast;

import com.elrsoft.streets.R;
import com.elrsoft.streets.fragment.core.CoreFragment;
import com.elrsoft.streets.util.GPSTracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

/**
 * Created by minos on 11.04.2017.
 */

@EFragment(R.layout.fragment_position)
public class PositionFragment extends CoreFragment {

    private GoogleMap googleMap;
    GPSTracker gps;
    double latitude;
    double longitude;

    @AfterViews
    protected void afviews() {
        //get position
        gps = new GPSTracker(getActivity().getApplicationContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }

        //create map
        try {
            if(null == googleMap){
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.map)).getMap();
                if(null == googleMap) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception){
            Log.e("myLogs", exception.toString());
        }
        //add my position on map
        googleMap.setMyLocationEnabled(true);

        //add camera(run exeption on genymotion emulator, comment this)
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(15)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.animateCamera(cameraUpdate);

        //add marker
        if(null != googleMap){
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .title("My Position")
                    .draggable(true)
            );
        }

    }

    @Override
    public void backPressed() {
        getActivity().finish();
    }

}

