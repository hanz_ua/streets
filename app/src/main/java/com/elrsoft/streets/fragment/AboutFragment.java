package com.elrsoft.streets.fragment;

import android.text.Html;
import android.widget.TextView;

import com.elrsoft.streets.R;
import com.elrsoft.streets.fragment.core.CoreFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by minos on 13.03.2017.
 */

@EFragment(R.layout.fragment_about)

public class AboutFragment extends CoreFragment {
    @ViewById
    TextView aboutText;
    @ViewById
    TextView tv_version;

    @AfterViews
    protected void setAboutText() {
        tv_version.setText(getString(R.string.tv_version));
        aboutText.setText(Html.fromHtml(getString(R.string.tv_about)));
    }


    @Override
    public void backPressed() {
        getActivity().finish();
    }
}
