package com.elrsoft.streets.controller;


import android.app.Fragment;
import android.app.FragmentManager;


/**
 * Created by hanz on 26.02.2016.
 */
public class NavigationFragmentController {


    public static void moveToNextFragment(FragmentManager myFragmentManager, int container, Fragment fragment, String tag) {
        if (myFragmentManager != null)
            myFragmentManager.beginTransaction().replace(container, fragment, tag).commit();

    }

    public static void addToNextFragment(FragmentManager myFragmentManager, int container, Fragment fragment, String tag) {
        myFragmentManager.beginTransaction().add(container, fragment, tag).commit();

    }

    public static void removeFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().remove(fragment).commit();
    }


}
