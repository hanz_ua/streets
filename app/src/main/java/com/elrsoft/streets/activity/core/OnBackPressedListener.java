package com.elrsoft.streets.activity.core;

/**
 * Created by hanz on 04.07.2016.
 */
public interface OnBackPressedListener {

   public void backPressed();
}
