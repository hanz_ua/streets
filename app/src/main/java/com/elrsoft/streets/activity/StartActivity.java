package com.elrsoft.streets.activity;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.elrsoft.streets.R;
import com.elrsoft.streets.activity.core.CoreActivity;
import com.elrsoft.streets.controller.NavigationFragmentController;
import com.elrsoft.streets.fragment.AboutFragment_;
import com.elrsoft.streets.fragment.PositionFragment_;
import com.elrsoft.streets.fragment.StartFragment_;
import com.elrsoft.streets.util.Resource;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.activity_main_drawer)
public class StartActivity extends CoreActivity {

    @ViewById(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @ViewById(R.id.nvView)
    NavigationView nvDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;


    @AfterViews
    void init() {
        NavigationFragmentController.moveToNextFragment(
                getFragmentManager(), R.id.mainContainer, StartFragment_.builder().build(), Resource.START_FRAGMENT);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerToggle = setupDrawerToggle();
        setupDrawerContent(nvDrawer);
        mDrawer.addDrawerListener(drawerToggle);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }


    public void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.position_menu:
                NavigationFragmentController.moveToNextFragment(
                        getFragmentManager(), R.id.mainContainer, PositionFragment_.builder().build(), Resource.POSITION_FRAGMENT);

                break;
            case R.id.places_menu:
                Toast.makeText(this, "my_places_menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.schedule_menu:
                Toast.makeText(this, "schedule_menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.route_menu:
                Toast.makeText(this, "route_menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.settings_menu:
                Toast.makeText(this, "settings_menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.about_menu:
                NavigationFragmentController.moveToNextFragment(
                        getFragmentManager(), R.id.mainContainer, AboutFragment_.builder().build(), Resource.ABOUT_FRAGMENT);
                break;
            default:
        }
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


}

