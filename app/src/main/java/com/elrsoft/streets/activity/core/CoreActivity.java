package com.elrsoft.streets.activity.core;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.WindowManager;


import java.util.ArrayList;
import java.util.List;
import com.annimon.stream.Stream;

import rx.Observable;
import rx.functions.Action1;

/**
 * Created by hanz on 04.07.2016.
 */
public abstract class CoreActivity extends AppCompatActivity {
    private final static String ON_BACK_PRESSED = "OnBackPressed";

    private Observable<String> myObservable = Observable.just(ON_BACK_PRESSED);
    public List<Action1<String>> backPressedListeners = new ArrayList<>();

    public void setBackPressedListener(Action1<String> action1) {
        backPressedListeners.add(action1);
    }

    public void removeBackPressedListener(Action1<String> action1) {
        backPressedListeners.remove(action1);
    }

    @Override
    public void onBackPressed() {
        Stream.of(backPressedListeners).forEach(list -> myObservable.subscribe(list));
    }


}
